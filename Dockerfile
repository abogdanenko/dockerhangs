FROM alpine:edge
VOLUME /var/lib/docker
RUN apk upgrade && apk add --no-cache docker
